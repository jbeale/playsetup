package controllers;

import models.Task;
import play.db.ebean.Model;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import java.util.List;
import play.data.*;
import static play.data.Form.form;
import static play.mvc.Results.ok;
import static play.mvc.Results.redirect;

/**
 * TaskController.java
 * @author jbeale-cengage
 */
public class TaskController extends Controller {
	public static Result listTasks() {
		List<Task> tasks = Task.find.all();
		return ok(listTasks.render(tasks));
	}
	public static Result create() {
		Form<Task> task = form(Task.class);
		return ok(taskForm.render(task));
	}
	
	public static Result save() {
		Form<Task> form = form(Task.class).bindFromRequest();
		Task task = form.get();
		task.save();
		return redirect(routes.TaskController.listTasks());
	}
}
